//package com.algaworks.osworks.api;
//
//import com.algaworks.osworks.api.dto.ClienteDTO;
//import com.algaworks.osworks.domain.model.Cliente;
//import org.mapstruct.InheritInverseConfiguration;
//import org.mapstruct.Mapping;
//
//import java.lang.reflect.Field;
//
//public interface ClienteMapper {
//    Cliente toCliente(ClienteDTO dto);    @Mapping(target = "anoutherField", ignore = true) // ignorar map
//     ClienteDTO toDto(Cliente model);
//
//    @Mapping(source = "type", target = "dataType") // map
//    Field clienteDtoToCliente(ClienteDTO dto); //para o relacionamento List fields
//
//    @InheritInverseConfiguration
//    ClienteDTO clienteToClienteDto(Field model); //para o relacionamento List fields
//
//}
