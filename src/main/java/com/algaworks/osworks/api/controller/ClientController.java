package com.algaworks.osworks.api.controller;


import com.algaworks.osworks.domain.model.entitys.ClienteEntity;
import com.algaworks.osworks.domain.repository.ClienteRepository;
import com.algaworks.osworks.domain.service.CadastroClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/clientes")
public class ClientController {

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private CadastroClienteService cadastroClienteService;

    @GetMapping
    public List<ClienteEntity> listar() {
        return clienteRepository.findAll();
    }

    @GetMapping("/{clienteId}")
    public ResponseEntity<ClienteEntity> buscar(@PathVariable String clienteId) {
        Optional<ClienteEntity> cliente = clienteRepository.findById(clienteId);

        if (cliente.isPresent()) {
            return ResponseEntity.ok(cliente.get());
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ClienteEntity adicionar(@Valid @RequestBody ClienteEntity cliente) {
        return cadastroClienteService.salvar(cliente);
    }

//    @PostMapping
//    @ResponseStatus(HttpStatus.CREATED)
//    public Cliente adicionar(@Valid @RequestBody Cliente cliente) {
//        return clienteRepository.save(cliente);
//    }

    @PutMapping("/{clienteId}")
    public ResponseEntity<ClienteEntity> atualizar(@Valid @PathVariable String clienteId,
                                                   @RequestBody ClienteEntity cliente) {
        if (!clienteRepository.existsById(clienteId)){
            return ResponseEntity.notFound().build();
        }

        cliente.setId(clienteId);
        cliente = cadastroClienteService.salvar(cliente);

        return ResponseEntity.ok(cliente);
    }

    @DeleteMapping("/{clienteId}")
    public ResponseEntity<Void> remover(@PathVariable String clienteId){
        if (!clienteRepository.existsById(clienteId)){
            return ResponseEntity.notFound().build();
        }

        cadastroClienteService.excluir(clienteId);

        return  ResponseEntity.noContent().build();

    }
}
