package com.algaworks.osworks.api.controller;


import com.algaworks.osworks.api.dto.ClienteDTO;
import com.algaworks.osworks.domain.service.SearchService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/search")
public class SearchClienteController {

    private static final String DATE_PATTERN = "yyyy/MM/dd";

    private final SearchService searchService;


    public SearchClienteController(SearchService example) {
        this.searchService = example;
    }


    @GetMapping
    public List<ClienteDTO> getCliente(@RequestParam(required = false)
                                        @DateTimeFormat(pattern = DATE_PATTERN) Date fromDate,
                                        @RequestParam(required = false)
                                        @DateTimeFormat(pattern = DATE_PATTERN) Date toDate,
                                        @RequestParam(required = false) String id,
                                        @RequestParam(required = false) String nome,
                                        @RequestParam(required = false) String email,
                                        @RequestParam(required = false) String telefone,
                                        Pageable pageable){
        return searchService.getCliente(fromDate, toDate, id, nome, email, telefone, pageable);
    }

    @PostMapping
    public void addCliente(@RequestBody ClienteDTO cliente) {
        searchService.addCliente(cliente);
    }

}
