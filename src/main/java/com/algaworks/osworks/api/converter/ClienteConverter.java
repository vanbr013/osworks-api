package com.algaworks.osworks.api.converter;

import com.algaworks.osworks.api.dto.ClienteDTO;
import com.algaworks.osworks.domain.model.entitys.ClienteEntity;

import java.util.List;
import java.util.stream.Collectors;

public class ClienteConverter {

    public static List<ClienteDTO> convertToClienteDTO(List<ClienteEntity> cliente) {
        return cliente.stream().map(s ->
                ClienteDTO.builder()
                        .id(s.getId())
                        .nome(s.getNome())
                        .email(s.getEmail())
                        .telefone(s.getTelefone())
                        .build()
        ).collect(Collectors.toList());
    }

    public static ClienteEntity convertToCliente(ClienteDTO clienteDTO) {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome(clienteDTO.getNome());
        return cliente;
    }

}
