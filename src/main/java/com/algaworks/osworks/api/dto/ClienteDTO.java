package com.algaworks.osworks.api.dto;

import lombok.Builder;
import lombok.Getter;

import java.util.Date;

@Builder
@Getter
public class ClienteDTO {

    private String id;

    private String nome;

    private String email;

    private String telefone;

}
