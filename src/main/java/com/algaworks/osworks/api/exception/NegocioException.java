package com.algaworks.osworks.api.exception;

import javassist.SerialVersionUID;

public class NegocioException extends RuntimeException {

    private static final long SerialVersionUID = 1L;

    public NegocioException(String message){
        super(message);
    }

}
