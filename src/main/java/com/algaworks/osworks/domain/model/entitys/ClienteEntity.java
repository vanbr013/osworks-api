package com.algaworks.osworks.domain.model.entitys;

import com.algaworks.osworks.domain.model.Auditable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.UUID;

@Entity
@Getter @Setter
@EqualsAndHashCode
@Table(name = "\"cliente\"", schema = "\"public\"")
public class ClienteEntity extends Auditable {

    private static final long serialVersionUID = 1L;



    @Id
    @Column(name = "cliente_id")
    private String id = UUID.randomUUID().toString();

    @NotBlank
    @Size(max = 60)
    @Column(name = "cliente_nome")
    private String nome;

    @NotBlank
    @Size(max = 255)
    @Email
    @Column(name = "cliente_email")
    private String email;

    @NotBlank
    @Size(max = 20)
    @Column(name = "cliente_telefone")
    private String telefone;

}
