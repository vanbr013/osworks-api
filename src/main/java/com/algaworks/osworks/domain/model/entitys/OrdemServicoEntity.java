package com.algaworks.osworks.domain.model.entitys;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter @Setter
@EqualsAndHashCode
@Table(name = "\"ordem_servico\"", schema = "\"public\"")
public class OrdemServicoEntity {

    private static final long serialVersionUID = 1L;

//Assitir minuto 19 da aula 3

    @Id
    @Column(name = "ordem_servico_id")
    private String id = UUID.randomUUID().toString();



    @NotBlank
    @ManyToOne
    @JoinColumn(name = "ordem_servico_cliente_id")
    private ClienteEntity cliente;

    @NotBlank
    @Column(name = "ordem_servico_descricao")
    private String descricao;

    @NotBlank
    @Column(name = "ordem_servico_preco")
    private BigDecimal preco;

    @NotBlank
    @Enumerated(EnumType.STRING)
    @Column(name = "ordem_servico_status")
    private StatusOrdemServico status;

    @Column(name = "ordem_servico_data_abertura")
    private LocalDateTime dataAbertura;

    @Column(name = "ordem_servico_data_finalizacao")
    private LocalDateTime dataFinalizacao;

}
