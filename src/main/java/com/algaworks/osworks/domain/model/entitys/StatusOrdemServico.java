package com.algaworks.osworks.domain.model.entitys;

public enum StatusOrdemServico {

    ABERTA, FINALIZADA, CANCELADA

}
