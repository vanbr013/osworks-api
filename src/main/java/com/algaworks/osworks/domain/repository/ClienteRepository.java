package com.algaworks.osworks.domain.repository;

import com.algaworks.osworks.domain.model.entitys.ClienteEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClienteRepository extends JpaRepository<ClienteEntity, String>, JpaSpecificationExecutor<ClienteEntity> {

    List<ClienteEntity> findByNome(String nome);
    List<ClienteEntity> findByNomeContaining(String nome);
    ClienteEntity findByEmail(String email);
}

