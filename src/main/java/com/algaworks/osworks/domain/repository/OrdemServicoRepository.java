package com.algaworks.osworks.domain.repository;

import com.algaworks.osworks.domain.model.entitys.OrdemServicoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrdemServicoRepository extends JpaRepository<OrdemServicoEntity, String> {
}
