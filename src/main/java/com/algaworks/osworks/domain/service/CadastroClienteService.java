package com.algaworks.osworks.domain.service;

import com.algaworks.osworks.api.exception.NegocioException;
import com.algaworks.osworks.domain.model.entitys.ClienteEntity;
import com.algaworks.osworks.domain.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CadastroClienteService {

    @Autowired
    public ClienteRepository clienteRepository;

    public ClienteEntity salvar(ClienteEntity cliente){
        ClienteEntity clienteExistente = clienteRepository.findByEmail(cliente.getEmail());
//        Vejo se já existe um cliente com este mesmo email e lanço exception se necessario
        if (clienteExistente != null && !clienteExistente.equals(cliente)){
            throw new NegocioException("Já existe um cliente cadastrado com este e-mail.");
        }

        return cliente;
    }

    public void excluir (String clienteId){
        clienteRepository.deleteById(clienteId);
    }
    
}
