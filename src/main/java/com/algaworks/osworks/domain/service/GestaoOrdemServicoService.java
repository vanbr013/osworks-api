package com.algaworks.osworks.domain.service;

import com.algaworks.osworks.domain.model.entitys.OrdemServicoEntity;
import com.algaworks.osworks.domain.model.entitys.StatusOrdemServico;
import com.algaworks.osworks.domain.repository.OrdemServicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class GestaoOrdemServicoService {

    @Autowired
    private OrdemServicoRepository ordemServicoRepository;

    public OrdemServicoEntity criar(OrdemServicoEntity ordemServicoEntity){
        ordemServicoEntity.setStatus(StatusOrdemServico.ABERTA);
        ordemServicoEntity.setDataAbertura(LocalDateTime.now());

        return ordemServicoRepository.save(ordemServicoEntity);

    }
}
