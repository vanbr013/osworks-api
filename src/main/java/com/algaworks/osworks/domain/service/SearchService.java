package com.algaworks.osworks.domain.service;

import com.algaworks.osworks.api.dto.ClienteDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

// Essa interface só serve para a ser implementada no IMPL SearchService
public interface SearchService {

    void addCliente(ClienteDTO cliente);


    List<ClienteDTO> getCliente(Date fromDate, Date toDate, String id, String nome, String email,
                                String telefone, Pageable pageable);
}
