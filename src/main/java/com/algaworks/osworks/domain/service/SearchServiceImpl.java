package com.algaworks.osworks.domain.service;

import com.algaworks.osworks.api.converter.ClienteConverter;
import com.algaworks.osworks.api.dto.ClienteDTO;
import com.algaworks.osworks.domain.model.entitys.ClienteEntity;
import com.algaworks.osworks.domain.repository.ClienteRepository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Predicate;
import java.util.Date;
import java.util.List;

@Service
public class SearchServiceImpl implements SearchService {

    private final ClienteRepository clienteRepository;

    public SearchServiceImpl(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }


    @Override
    public void addCliente(ClienteDTO cliente) {
        clienteRepository.save(ClienteConverter.convertToCliente(cliente));
    }

    @Override
    public List<ClienteDTO> getCliente(Date fromDate, Date toDate, String id, String nome, String email,
                                       String telefone, Pageable pageable) {
        List<ClienteEntity> clientes = clienteRepository.findAll((Specification<ClienteEntity>) (root, cquery, cbuilder) -> {
            Predicate p = cbuilder.conjunction();
            if (!StringUtils.isEmpty(id)) {
                p = cbuilder.and(p, cbuilder.like(root.get("id"), "%" + id + "%"));
            }
            if (!StringUtils.isEmpty(nome)) {
                p = cbuilder.and(p, cbuilder.like(root.get("nome"), "%" + nome + "%"));
            }
            if (!StringUtils.isEmpty(email)) {
                p = cbuilder.and(p, cbuilder.like(root.get("email"), "%" + email + "%"));
            }
            if (!StringUtils.isEmpty(telefone)) {
                p = cbuilder.and(p, cbuilder.like(root.get("telefone"), "%" + telefone + "%"));
            }
            cquery.orderBy(             cbuilder.desc(root.get("id")),
                                        cbuilder.asc(root.get("nome")),
                                        cbuilder.asc(root.get("email")),
                                        cbuilder.asc(root.get("telefone")));
            return p;
        }, pageable).getContent();
        return ClienteConverter.convertToClienteDTO(clientes);
    }

}
