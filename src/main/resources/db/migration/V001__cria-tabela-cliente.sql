CREATE TABLE cliente(
    cliente_id VARCHAR(255),
    cliente_nome VARCHAR(60) not null,
    cliente_email VARCHAR(255) not null,
    cliente_telefone VARCHAR(20) not null,
    PRIMARY KEY (cliente_id)
);