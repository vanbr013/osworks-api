CREATE TABLE ordem_servico(
    ordem_servico_id VARCHAR not null,
    ordem_servico_cliente_id VARCHAR not null,
    ordem_servico_descricao TEXT not null,
    ordem_servico_preco DECIMAL(10,2) not null,
    ordem_servico_status VARCHAR(20) not null,
    ordem_servico_data_abertura TIMESTAMP WITH TIME ZONE not null,
    ordem_servico_data_finalizacao TIMESTAMP WITH TIME ZONE not null,

    PRIMARY KEY (ordem_servico_id)
);

ALTER TABLE ordem_servico ADD CONSTRAINT fk_ordem_servico_cliente
FOREIGN KEY (ordem_servico_cliente_id) REFERENCES cliente (cliente_id);